import React, { Component } from 'react'
import PropTypes from 'prop-types'
import './ChatInput.css'

import { Row, Col, Container } from 'react-bootstrap';

class ChatInput extends Component {
    static propTypes = {
        onSubmitMessage: PropTypes.func.isRequired,
    }
    state = {
        message: ''
    }

    render() {
        return (
            <div className="chatInput">
                <form className="input"
                    action="."
                    onSubmit={e => {
                        e.preventDefault()
                        this.props.onSubmitMessage(this.state.message)
                        this.setState({ message: '' })
                    }}
                >
                    <Container fluid="true">
                            <Row>
                                <Col sm={10}>
                                    <input
                                        type="text"
                                        placeholder={'Enter message...'}
                                        value={this.state.message}
                                        onChange={e => this.setState({ message: e.target.value })}
                                    />
                                </Col>
                                <Col sm={2}>
                                    <input type="submit" value={'Send'} />
                                </Col>
                            </Row>
                    </Container>
                </form>
            </div>
        )
    }
}

export default ChatInput