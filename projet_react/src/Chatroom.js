import React, { Component } from 'react'
import './Chatroom.css';
import Message from './Message.js';
import ChatInput from './ChatInput'


const URL = "wss://imr3-react.herokuapp.com";

class Chatroom extends Component {
    state = {
        name: 'Bob',
        messages: [],
    }

    ws = new WebSocket(URL)

    componentDidMount() {
        this.ws.onopen = () => {
            // on connecting, do nothing but log it to the console
            console.log('connected')
        }

        this.ws.onmessage = evt => {
            // on receiving a message, add it to the list of messages
            const message = JSON.parse(evt.data)
            console.log(message)
            this.addMessage(message)
            this.updateScroll();
        }

        this.ws.onclose = () => {
            console.log('disconnected')
            // automatically try to reconnect on connection loss
            this.setState({
                ws: new WebSocket(URL),
            })
        }
    }

    updateScroll() {
        console.log("Scroll " + 150);
        var element = document.getElementById("messages");
        element.scrollTop = element.scrollHeight;
    }

    addMessage = message => {
        this.setState({ messages: this.state.messages.concat(message) })
    }

    submitMessage = messageString => {
        // on submitting the ChatInput form, send the message, add it to the list and reset the input
        const message = { name: this.state.name, message: messageString }
        this.ws.send(JSON.stringify(message))
    }

    render() {
        const {messages,name} = this.state;
        return (
            <div className="chatroom">
                <h3> Chat</h3>
                <label className="pseudo" htmlFor="name">
                    Name:&nbsp;
                    <input
                        type="text"
                        id={'name'}
                        placeholder={'Enter your name...'}
                        value={this.state.name}
                        onChange={e => this.setState({ name: e.target.value })}
                    />
                </label>
                <ul className="messages" id="messages">
                    {
                        messages.map((chat,index) =>
                            <Message chat={chat} user={name} key={index}/>
                        )
                    }
                </ul>
                <ChatInput
                    ws={this.ws}
                    onSubmitMessage={messageString => this.submitMessage(messageString)}
                />
            </div>
        )
    }
}

export default Chatroom
