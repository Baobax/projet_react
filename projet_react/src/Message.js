import React from 'react';

const Message = ({chat, user}) => (
    <li className={`message ${user === chat.name ? "right" : "left"}`}>
        {user !== chat.name}
        {chat.message}
    </li>
);

export default Message;