import React from 'react';
import { Player } from 'video-react';
import "../node_modules/video-react/dist/video-react.css";

import { Chapitres } from './Chapitres'

export class Video extends React.Component {
    constructor(props) {
        super(props);
		this.state = {
			data_loaded: false,
			film: []
		};
    }

    handleChapClick = pos => {
		this.player.seek(pos);
	}
	
	handleStateChange(state) {
		for (let i = 0; i < this.state.film.Waypoints.length - 1; i++) {
			let waypoint = this.state.film.Waypoints[i];
			if (state.currentTime >= waypoint.timestamp
				&& state.currentTime < this.state.film.Waypoints[i+1].timestamp) {
				var coords = [waypoint.lat, waypoint.lng];
				var desc = waypoint.label;
				this.props.showMarker(coords, desc);
				break;
			}
		}

		for (let i = 0; i < this.state.film.Keywords.length - 1; i++) {
			let keyword = this.state.film.Keywords[i];
			if (state.currentTime >= keyword.pos
				&& state.currentTime < this.state.film.Keywords[i+1].pos) {
				this.props.showKeywords(keyword.data);
				break;
			}
		}
	}

    render() {
    	const {data_loaded, film} = this.state;
        if(data_loaded){
            return (
            	<div id="videoPlayer">
				    <Player ref={player => {
						this.player = player;
						}}>
						<source src={film.Film.file_url}/>
				    </Player>
				    <Chapitres handleClick={this.handleChapClick} data={film.Chapters}/>
			    </div>
            );
        } else {
            return (
                <div>
                    <p>Data loading...</p>
                </div>
            );
        }
    }

	componentDidMount() {
		fetch("https://imr3-react.herokuapp.com/backend")
			.then(response => response.json())
			.then((jsonData) => {
				// jsonData is parsed json object received from url
				this.setState({
					data_loaded: true,
					film: jsonData
				});
				this.player.subscribeToStateChange(this.handleStateChange.bind(this));
			})
			.catch((error) => {
				// handle your errors here
				console.error(error)
			});
	}
}