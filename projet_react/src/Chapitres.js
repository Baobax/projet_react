import React from 'react';
import { Button } from 'react-bootstrap';

export class Chapitres extends React.Component {
    render() {
    	var chapList = this.props.data.map(item => {
							return (
		    					<React.Fragment key={item.pos}>
									<Button onClick={() => this.props.handleClick(item.pos)} className="chapitre">{item.title}</Button>
		    					</React.Fragment>
							);
						});

    	return (
		    <div id="chapitres">
		    	<h4>Chapitres :</h4>
		    	{ chapList }
		    </div>
        );
    }
}