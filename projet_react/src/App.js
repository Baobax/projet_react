import React from 'react';
import './App.css';

import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import { Video } from './Video'
import { Carte } from './Carte'
import { Keyword } from './Keyword'
import Chatroom from './Chatroom.js';
import { Tabs, Tab, Row, Col, Container } from 'react-bootstrap';

export class App extends React.Component {
  constructor(props) {
      super(props);
      this.state = {
        coords: [0.0, 0.0],
        desc: "",
        tab: "chat",
        keywords: []
      }
  }

  updateMapCoords = (coords, desc) => {
    if (this.state.coords !== coords) {
      this.setState({
        coords: coords,
        desc: desc
      });
    }
  }

  showKeywords = (keywords) => {
    if (this.state.keywords !== keywords) {
      this.setState({
        keywords: keywords
      });
    }
  }

  onSelect() {
    /* Pour régler le conflit de css entre bootstrap et leaflet quand on change de tab
       On envoie un événement de type resize pour forcer la map à recalculer sa taille */
    window.dispatchEvent(new Event('resize'));
  }

  render() {
    return (
      <Container fluid="true">
        <Row>
          <Col sm={8}>
            <Video showMarker={this.updateMapCoords} showKeywords={this.showKeywords}/>
          </Col>
          <Col sm={4}>
            <Tabs onSelect={this.onSelect.bind(this)} defaultActiveKey="chat">
              <Tab eventKey="chat" title="Chat">
                <Chatroom />
              </Tab>
              <Tab eventKey="map" title="Map">
                <Carte coords={this.state.coords} description={this.state.desc}/>
              </Tab>
              <Tab eventKey="keywords" title="Keywords">
                <Keyword keyword={this.state.keywords}/>
              </Tab>
            </Tabs>
          </Col>
        </Row>
      </Container>
    );
  }
}

export default App;