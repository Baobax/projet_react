import React from 'react';


export class Keyword extends React.Component {
    render() {
    	var keywordList = this.props.keyword.map(item => {
							return (
		    					<React.Fragment key={item.title}>
                                    <a href={item.url}>{item.title}</a><br/>
		    					</React.Fragment>
							);
                        });

    	return (
            <div>
                {keywordList}
            </div>
        );
    }
}