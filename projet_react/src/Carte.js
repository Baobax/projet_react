import React from 'react';

import { Map as LeafletMap, TileLayer, Marker, Popup } from 'react-leaflet';


export class Carte extends React.Component {
    render() {
		var coords = this.props.coords;
		var description = this.props.description;

    	return (
			<div>
				<LeafletMap ref="map" id="mapid" center={coords} zoom={13}>
					<TileLayer
					attribution='&amp;copy <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
					url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
					/>
					<Marker position={coords}>
						<Popup>
							{description}
						</Popup>
					</Marker>
				</LeafletMap>
			</div>
        );
	}
}